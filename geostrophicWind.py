# MTMW12 assignment 3. Hilary Weller 1 October 2020
# Python3 code to numerically differentiate the pressure in order to calculate
# the geostrphic wind relation using 2-point differencing and
# compare with the analytic solution and plot

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWind():
    """Calculate the geostrophic with analytically and numerically and plot"""
    
    # Resolution and size of the domain
    N = 10               # the number of intervals to divide space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N # the length of the spacing

    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)

    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Two-point differences', \
               ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent.pdf')
    plt.show()

    # plot the errors
    plt.plot(y/1000, u_2point - uExact, '*k--',
             label='Two-point differences',
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsCent.pdf')
    plt.show()

    # 3-point differences at end-points
    u_2ndOrder = geoWind(gradient_2ndOrder(p, dy), physProps)

    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2ndOrder, '*k--', label='2nd order differences', \
               ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWind3pt.pdf')
    plt.show()

    # plot the errors
    plt.plot(y/1000, u_2ndOrder - uExact, '*k--',
             label='2nd order differences',
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrors3pt.pdf')
    plt.show()

if __name__ == "__main__":
    geostrophicWind()

