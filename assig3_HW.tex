\documentclass[12pt]{article}

% compile this latex file with "pdflatex assig3_HW.tex"
% In order to compile this file you will either need to produce graphics files
% with the correct names or comment out the \includegraphics commands

% additional latex packages to use
\usepackage[a4paper, left=2.2cm,top=1.5cm, right=2.2cm,bottom=1.5cm,]{geometry}
\usepackage{times, graphicx, amsmath, mathtools}
\usepackage{url,multirow,xfrac}

% not so much space around floats
\renewcommand{\floatpagefraction}{0.95}
\renewcommand{\textfraction}{0}
\renewcommand{\topfraction}{1}
\renewcommand{\bottomfraction}{1}

\begin{document}
\thispagestyle{empty}

\title{MTMW12, Assignment 3, Numerical Differentiation}
\author{Hilary Weller}
\maketitle

The code is at \url{https://gitlab.act.reading.ac.uk/sws02hs/MTMW12_assig3}.

\begin{enumerate}
\item\label{q:diff} The wind, $u$, is evaluated from the pressure, $p$, by numerically differentiating the geostrophic wind relation:
\begin{equation}
u = -\frac{1}{\rho f}\frac{dp}{dy}
\label{eq:geoWind}
\end{equation}
where the pressure, $p$, is given by:
\begin{equation}
p = p_a + p_b \cos\frac{y\pi}{L}
\label{eq:cosPressure}
\end{equation}
where $p_a=10^5 \text{Pa}$, $p_b=200 \text{Pa}$, $f = 10^{-4}\text{s}^{-1}$, $\rho=1.2 \text{kg} \text{m}^{-3}$, $L = 2.4\times 10^6 \text{m}$ and $y:0\rightarrow 10^6 \text{m}$. The domain is divided into $N=10$ equal intervals. The exact $u$ is:
\begin{equation}
u_e = \frac{p^\prime\pi}{\rho f L}\sin\frac{y\pi}{L}.
\label{eq:exactU}
\end{equation}
The wind is evaluated numerically from the pressure gradient using the centred, second-order, two-point finite difference formula away from the end-points of the domain and using one-sided, first-order approximations at the end points to calculate the pressure gradients. The exact and numerically evaluated winds are shown in figure \ref{fig:u2pt} on the left and the errors are shown on the right. Code for generating these figures in in file \url{geostrophicWind.py}.

\begin{figure}[htb]
\includegraphics[width=0.49\linewidth]{plots/geoWindCent.pdf}
\includegraphics[width=0.49\linewidth]{plots/geoWindErrorsCent.pdf}
\caption{The analytic wind from the geostrophic relation and the numerical approximation calculated using two-point differences.}
\label{fig:u2pt}
\end{figure}

The errors are higher at the end points because the finite difference approximations are first-order accurate at the end points whereas they are second order accurate away from the end points. 

{\it Note:} For the next part {\it``Does the code behave as expected''} there is not one definitive answer. I provide one plausible answer. Other answers which include some analysis of the results could also gain full marks.

We can check if the expected order of accuracy is achieved by the numerical implementation by changing the resolution and inspecting the change in errors. The errors at $y=0$m and $y=5\times 10^5$m (the middle of the domain) for resolutions using $N=10, 20, 40$ and $N=80$ are shown in figure \ref{fig:Q1order}. This shows that errors converge proportional to $\Delta y$ at $y=0$, where a first-order approximation is used and proportional to $\Delta y^2$ in the middle of the domain, where a second-order approximation is used, as expected. The code for generating this figure is in file \url{order.py}.

\begin{figure}[htb]
\centering
\includegraphics[width=0.75\linewidth]{plots/Q1order.pdf}
\caption{Error as a function of $\Delta y$ at and end-point ($y=0$ m) and in the middle of the $y$-range. The errors are compared with lines with gradients of one and two.}
\label{fig:Q1order}
\end{figure}


\item\label{q:alt} For question \ref{q:alt}, two alternative answers are provided. For full marks, it is only necessary to present one answer, which may be different from both of these.

\begin{enumerate}
\item The largest errors in the numerical differentiation in question \ref{q:diff} are at the end points of the domain. Therefore the simplest way to improve the differentiation would be to use a more accurate differentiation at the end points. The calculation of the end points can be improved by using un-centred, 3 point, 2nd-order finite difference formulae:
\begin{equation}
\frac{dp}{dy}_0 = \frac{-3p_{0} + 4p_{1} - p_{2}}{2\Delta y}
\end{equation}
and
\begin{equation}
\frac{dp}{dy}_N = \frac{p_{N-2} - 4p_{N-1} + 3p_{N}}{2\Delta y}.
\end{equation}
The wind calculated using these approximations at the end points and centred finite differences away from the end points are shown in figure \ref{fig:u3pt} and they are compared with the analytic solution. The code for this figure is also in file \url{geostrophicWind.py}. The calculation of the end points is now much more accurate, the errors are less than 0.012 in magnitude at both end points in comparison to 0.04 and -0.14 using the first order approximations in question \ref{q:diff}.

\begin{figure}[htb]
\includegraphics[width=0.49\linewidth]{plots/geoWind3pt.pdf}
\includegraphics[width=0.49\linewidth]{plots/geoWindErrors3pt.pdf}
\caption{The analytic wind from the geostrophic relation and the numerical approximation calculated using two-point differences and three-point differences at the ends.}
\label{fig:u3pt}
\end{figure}

We can also confirm that these errors are second order accurate as expected by calculating errors for two values of $N$. For both end-points, these are shown in table \ref{tab:endPointErrors} along with the calculated order of convergence.

\begin{table}[htb]\centering
\begin{tabular}{c|c|c|l|c|l}
$y$ (m) & $N$ & 2-point differences & order & 3-point differences & order\\
\hline
0 & 10 & 0.143 & \multirow{2}{*}{ $\Big. \Big\}0.998  $} & 0.00122 & \multirow{2}{*}{ $\Big. \Big\} 3.00 $}\\
0 & 20 & 0.0714 & & 0.000153 \\
$10^6$ & 10 & -0.0429 & \multirow{2}{*}{ $\Big. \Big\}1.10$} & 0.0116 & \multirow{2}{*}{ $\Big. \Big\} 1.97 $}\\
$10^6$ & 20 & -0.0200 && 0.00296 \\
\end{tabular}
\caption{Errors at the end points calculated using different approximations and resolutions.}
\label{tab:endPointErrors}
\end{table}

Using the three-point difference, the error at $y=0$ appears to converge with third-order accuracy whereas at $y=10^6~$m, the error converges with second-order accuracy as expected. {\it (A better answer would explain the third-order convergence.)}

\item Improved accuracy for cost in comparison to the numerical gradients used in question \ref{q:diff} can be achieved by calculating pressure gradients at the mid-points in between where the values where pressure are defined. This then leads to more compact centred differences:
\begin{equation}
\frac{\partial p}{\partial y}_{j+\sfrac{1}{2}} = \frac{p_{j+1} - p_{j}}{\Delta y}.
\end{equation}
These are expected to be more accurate because we are calculating the gradient over a smaller interval; $\Delta y$ rather than $2\Delta y$. The pressure gradient calculated using this method is calculated at locations:
\begin{equation}
y_{j+\sfrac{1}{2}} = (j+\sfrac{1}{2})\Delta y ~\text{ for }~ j=0\cdots N-1
\end{equation}
and the resulting geostrophic wind is shown in figure \ref{fig:uMid} in comparison to the analytic solution. The code for this calculation is in file \url{geoWindMidPoints.py}. The errors are also compared with the second-order co-located differences. The errors are approximately four times smaller, consistent with the halving of the effective resolution using these second-order methods. This is therefore a large gain in accuracy throughout the domain for no additional cost. 

\begin{figure}[htb]
\includegraphics[width=0.49\linewidth]{plots/geoWindMid.pdf}
\includegraphics[width=0.49\linewidth]{plots/geoWindErrorsMid.pdf}
\caption{The analytic wind from the geostrophic relation and the numerical approximation calculated using mid-point differences. The errors are compared with the errors from the second-order co-located differences.}
\label{fig:uMid}
\end{figure}


\end{enumerate}
\end{enumerate}
\end{document}

