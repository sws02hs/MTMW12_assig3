# MTMW12 assignment 3. Hilary Weller 1 October 2020
# Python3 code to numerically differentiate the pressure in order to calculate
# the geostrphic wind.
# Calculations at mid-points and compare with co-located calculation

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geoWindMid():
    """Calculate the geostrophic using mid-point differences and compare
       with analytic solution"""

    # Calculate the geostrophic wind at mid-points between pressure points
    N = 10
    ymin = 0.
    ymax = 1e6
    dy = (ymax - ymin)/N # the length of the spacing

    # The spatial dimension, y and ymid
    y = np.linspace(ymin, ymax, N+1)
    yMid = np.linspace(ymin + 0.5*dy, ymax - 0.5*dy, N)

    # The pressure and wind
    p = pressure(y, physProps)
    uMid = geoWind(gradient_midPoint(p, dy), physProps)
    uExactMid = uGeoExact(yMid, physProps)
    
    # Plot the wind in comparison to analytic
    font = {'size' : 14}
    plt.rc('font', **font)
    plt.plot(yMid/1000, uExactMid, 'k-', label='Exact')
    plt.plot(yMid/1000, uMid, '*k--', label='mid-point differences', \
               ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindMid.pdf')
    plt.show()

    # plot the errors in comparison to co-located errrors
    u = geoWind(gradient_2ndOrder(p, dy), physProps)
    plt.plot(y/1000, u - uGeoExact(y,physProps), 'ok-',
             label='2nd order differences',
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(yMid/1000, uMid - uExactMid, '*k--',
             label='mid-point differences',
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle='-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsMid.pdf')
    plt.show()

geoWindMid()

