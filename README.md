The Python3 code for Hilary Weller's answer to MTMW12 assignment 3. To create all the plots for the report, run

mkdir plots
python3 geostrophicWind.py
python3 geoWindMidPoints.py
python3 order.py

To compile the report:
pdflatex assig3_HW.tex