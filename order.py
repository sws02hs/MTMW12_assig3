# MTMW12 assignment 3. Hilary Weller 26 Sept 2019
# Python3 code to numerically differentiate the pressure in order to calculate
# the geostrphic wind.
# Calculations at different resolutions to plot order of convergence.

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geoWindOrder():
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]

    # Errors versus resolution. One error in the middle of the y domain and
    # one at one end.
    # Different values of N to calculate errors for
    Ns = np.array([10, 20, 40, 80])
    # Values of dy for these values of N
    dys = (ymax - ymin)/Ns
    # Save the end point and mid point error for these resolutions
    errorsMid = np.zeros(len(Ns))
    errorsEnd = np.zeros(len(Ns))
    # Calculate errors for each resolution
    for i in range(len(Ns)):
        y = np.linspace(ymin, ymax, Ns[i]+1)
        p = pressure(y, physProps)
        u = geoWind(gradient_2point(p, dys[i]),physProps)
        uExact = uGeoExact(y,physProps)
        errorsEnd[i] = abs(u[0] - uExact[0])
        # Location of the mid-point
        imid = int(Ns[i]/2)
        errorsMid[i] = abs(u[imid] - uExact[imid])

    # Lines for 1st and 2nd order accuracy
    e0 = 5e-5
    dy = [dys[0]/1000, dys[-1]/1000]
    first = np.array([dy[0]/dy[1]*e0, e0])
    second = np.array([(dy[0]/dy[1])**2*e0, e0])

    # Plot the errors as a function of resolution (on log-log scale)
    font = {'size' : 14}
    plt.rc('font', **font)
    plt.loglog(dys/1000, errorsEnd, '*k--',  label='at $y=0$ m')
    plt.loglog(dys/1000, errorsMid, 'ok:', label='at $y=500$ km')
    plt.loglog(dy, first, 'k-', label='First')
    plt.loglog(dy, second, 'k--', label='Second')
    plt.xlim([10,110])
    plt.legend(loc='best')
    plt.xlabel('$\Delta y$ (km)')
    plt.ylabel('error (m/s)')
    plt.tight_layout()
    plt.savefig('plots/Q1order.pdf')
    plt.show()

geoWindOrder()

